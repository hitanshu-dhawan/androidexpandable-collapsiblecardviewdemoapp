package com.hitanshudhawan.expandablecollapsiblecardview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private CardView cardView;
    private ImageView arrowImageView;
    private LinearLayout collapsibleLayout;
    private boolean collapsed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardView = (CardView) findViewById(R.id.card_view);
        arrowImageView = (ImageView) findViewById(R.id.arrow_image_view);
        collapsibleLayout = (LinearLayout) findViewById(R.id.collapsible_layout);
        collapsed = true;

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransitionManager.beginDelayedTransition(cardView);
                if (collapsed) {
                    arrowImageView.setImageResource(R.mipmap.ic_arrow_drop_up_black_24dp);
                    collapsibleLayout.setVisibility(View.VISIBLE);
                }
                else {
                    arrowImageView.setImageResource(R.mipmap.ic_arrow_drop_down_black_24dp);
                    collapsibleLayout.setVisibility(View.GONE);
                }
                collapsed = !collapsed;
            }
        });
    }

}
